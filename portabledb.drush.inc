<?php

/**
 * @file
 * Generator and parser of portable database dumps.
 */

/**
 * Implements hook_drush_command().
 */
function portabledb_drush_command() {
  $items['portabledb-import'] = array(
    'description' => dt('Import a portable dump.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
    'core' => array(7),
    'arguments' => array(
      'filename' => dt('The file to import.'),
    ),
  );
  $items['portabledb-export'] = array(
    'description' => dt('Generate a portable dump of the current site.'),
    'core' => array(7),
    'arguments' => array(),
  );
  return $items;
}

/**
 * Drush command validation hook for 'portabledb-import'.
 */
function drush_portabledb_import_validate($filename = NULL) {
  if (!isset($filename)) {
    return drush_set_error('DRUSH_PORTABLEDB_NO_FILENAME', dt('Missing argument: filename.'));
  }

  if (!file_exists($filename) || !is_readable($filename)) {
    return drush_set_error('DRUSH_PORTABLEDB_NOT_FOUND', dt('Unable to open "!filename" for reading.', array('!filename' => $filename)));
  }
}

/**
 * Drush command hook for 'portabledb-import'.
 */
function drush_portabledb_import($filename = NULL) {
  $connection = Database::getConnection();

  // Both MySQL and SQLite have a dependency on unicode.inc.
  require_once DRUPAL_ROOT . '/includes/unicode.inc';

  // MySQL and derivatives needs a special mode to retain zeros in autoincrement
  // columns.
  if (Database::getConnection()->databaseType() == 'mysql') {
    $connection->query("SET sql_mode='ANSI,TRADITIONAL,NO_AUTO_VALUE_ON_ZERO'");
  }

  try {
    include $filename;
  }
  catch (Exception $e) {
    $ret['#abort'] = array('success' => FALSE, 'query' => $e->getMessage());
    drush_set_error('DRUPAL_EXCEPTION', $e->getMessage());
  }
}

/**
 * Drush command hook for 'portabledb-export'.
 */
function drush_portabledb_export() {
  $handle = STDOUT;

  $date = gmdate('r');

  // Generate the header.
  fputs($handle, <<<ENDOFHEADER
<?php

/**
 * @file
 * Portable Drupal database dump.
 *
 * @date $date.
 */


ENDOFHEADER
);

  // Get the current schema, order it by table name.
  $schema = drupal_get_schema();
  ksort($schema);

  // Export all the tables in the schema.
  foreach ($schema as $table => $data) {
    // Dump the table structure.
    fputs($handle, "\$connection->schema()->dropTable(" . portabledb_var_export($table) . ");\n");
    fputs($handle, "\$connection->schema()->createTable(" . portabledb_var_export($table) . ", " . portabledb_var_export($data) . ");\n");

    // Don't output values for those tables.
    if (substr($table, 0, 5) == 'cache' || $table == 'sessions' || $table == 'watchdog') {
      fputs($handle, "\n");
      continue;
    }

    // Prepare the export of values.
    $result = db_select($table, NULL, array('fetch' => PDO::FETCH_ASSOC))
      ->fields($table);
    $insert = '';
    $seen = FALSE;
    foreach ($result->execute() as $record) {
      if (!$seen) {
        $seen = TRUE;
        // Output the db_insert() header.
        fputs($handle, "\$connection->insert(" . portabledb_var_export($table) . ")->fields(". portabledb_var_export(array_keys($data['fields'])) .")\n");
      }

      fputs($handle, '->values('. portabledb_var_export($record) .")\n");
    }

    // Output the db_insert() footer.
    if ($seen) {
      fputs($handle, "->execute();\n");
    }

    fputs($handle, "\n");
  }
}

/**
 * Drupal-friendly var_export().
 *
 * This is a fork of Drupal 7 portabledb_var_export(), to workaround a few bugs.
 *
 * @param $var
 *   The variable to export.
 * @param $prefix
 *   A prefix that will be added at the begining of every lines of the output.
 * @return
 *   The variable exported in a way compatible to Drupal's coding standards.
 */
function portabledb_var_export($var, $prefix = '') {
  if (is_array($var)) {
    if (empty($var)) {
      $output = 'array()';
    }
    else {
      $output = "array(\n";
      // Don't export keys if the array is non associative.
      $export_keys = array_values($var) != $var;
      foreach ($var as $key => $value) {
        $output .= '  ' . ($export_keys ? portabledb_var_export($key) . ' => ' : '') . portabledb_var_export($value, '  ', FALSE) . ",\n";
      }
      $output .= ')';
    }
  }
  elseif (is_bool($var)) {
    $output = $var ? 'TRUE' : 'FALSE';
  }
  elseif (is_string($var)) {
    $line_safe_var = str_replace("\n", '\n', $var);
    if (strpos($var, "\n") !== FALSE || strpos($var, "'") !== FALSE) {
      // If the string contains a line break or a single quote, use the
      // double quote export mode. Encode backslash and double quotes and
      // transform some common control characters.
      $var = str_replace(array('\\', '"', "\n", "\r", "\t", '$'), array('\\\\', '\"', '\n', '\r', '\t', '\$'), $var);
      $output = '"' . $var . '"';
    }
    else {
      $output = "'" . $var . "'";
    }
  }
  else {
    $output = var_export($var, TRUE);
  }

  if ($prefix) {
    $output = str_replace("\n", "\n$prefix", $output);
  }

  return $output;
}
